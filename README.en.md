# 开源奥义

#### Description
我是谁？生命是什么，认识你自己，认识家庭，认识家谱，认识封神榜，认识三百六十五路正神，认识爹娘爷叔
我从哪里来？世界那么大，我想去看看，遂有山海经，坤舆万国图，诸天星象图
我到哪里去？追问时空宇宙，何处是归途，学善成人，正冠修身，成家齐家，建房治国，处世和人平天下，开天辟地，走向深空，建设家园。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
